$('.progress').css('visibility', 'hidden');
$(document).ready(function () {
    $(document).on('click', '#btn-accept', function (evt) {
        evt.preventDefault();        
        if ($('#form-account').valid()) {  
            $('.progress').css('visibility', 'visible');
            $('#btn-accept').attr('disabled', true);
            $('.is-input').prop('readonly', true);
            var xhr = $.ajax({
                url: '/ethWebjs/create-user',
                type: 'POST',
                data: $('#form-account').serialize(),
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    $('.is-input').prop('readonly', false);
                    $('#btn-accept').attr('disabled', false);
                    $('.progress').css('visibility', 'hidden');
                    if (data.status == true) {                                           
                        $('.is-input').val('');
                        Materialize.toast(data.message, 4000, 'rounded blue');
                        window.setTimeout(function(){window.location='/ethWebjs';},2000);                        
                    } else {
                        Materialize.toast(data.message, 4000, 'rounded red');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    try {
                        var msg = JSON.parse(jqXHR.responseText);
                        $('.is-input').prop('readonly', false);
                        $('#btn-accept').attr('disabled', false);
                        $('.progress').css('visibility', 'hidden');
                        Materialize.toast(msg.message, 4000, 'rounded red');
                    } catch (err) {
                        $('.is-input').prop('readonly', false);
                        $('#btn-accept').attr('disabled', false);
                        $('.progress').css('visibility', 'hidden');
                        console.log(err);
                    }
                }
            });
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }

        $(document).on('click', '#btn-cancel', function (evt) {
            evt.preventDefault();
            xhr.abort();
            $('.progress').css('visibility', 'hidden');
            $('#btn-accept').attr('disabled', false);
            $('.is-input').prop('readonly', false);
        });
    });
});