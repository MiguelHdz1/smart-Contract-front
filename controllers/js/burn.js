$(document).ready(function () {

    //addSelect('txt_burn', 'Elige una cuenta');
    $('#burnUd').on('click', function (evt) {
        evt.preventDefault();
        $('#modalBurn').modal('open');
    });
    
    $('#txt_burn').on('change',function(){
        $('#txt_burn_v').val($('#txt_burn_v').val());
    });

    $('#accept_burn').on('click', function (evt) {
        evt.preventDefault();
        if ($('#formBurn').valid()) {
            if (burn($('#amount_burn').val(), $('#txt_burn').val(), $('#password_burn').val())) {
                $('#modalBurn').modal('close');
            }
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
});