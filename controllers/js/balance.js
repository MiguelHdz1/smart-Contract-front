$(document).ready(function () {    
    
    $('#checkBalance').on('click', function (evt) {
        evt.preventDefault();
        showAccounts();
        $('#modal1').modal('open');
        $('.det').on('click',function(evt){
            evt.preventDefault();
            $('#modal1').modal('close');
            $('#details').modal('open');            
            var account =$(this).attr('data-bind').toString();
            $('#span-account-mobile').text('Cuenta: '+account.substr(0,14)+' '+account.substr(14,14)+' '+account.substr(28,14));
            $('#span-account').text('Cuenta: '+account);
            $('#span-balance').text('Saldo: '+(checkBalances($(this).attr('data-bind'))/decimals).toFixed(2));            
        });
        
        $('#close-info').on('click',function(){  
            $('#details').modal('close'); 
            $('#modal1').modal('open');            
        });
    });
});