$(document).ready(function (){
    //addSelect('txt_burnMltsg', 'Elige una cuenta');
    
    $('#burnMltsg').on('click',function (evt){
        evt.preventDefault();
        $('#modalBurnMltsg').modal('open');
    });
    
    $('#accept_burnMltsg').on('click',function (evt){
        evt.preventDefault();
        
        if($('#formBurnMltsg').valid()){
            if(burn_multisig($('#amount_burnMltsg').val(),$('#txt_burnMltsg').val(),$('#password_burnMltsg').val())){
                $('#modalBurnMltsg').modal('close');
            }
        }else{
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }        
    });
});