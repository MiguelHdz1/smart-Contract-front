$(document).ready(function(){
    $(document).on('click','#btn-logout',function(evt){
        evt.preventDefault();
        $('#modal-logout').modal('open');
    });
    
    $(document).on('click','#accept_logout',function(evt){
        evt.preventDefault();
        $('#close-session').submit();
    });
});