$(document).ready(function () {
    $('#newAcc').on('click', function (evt) {
        evt.preventDefault();
        $('#modalNew').modal('open');
    });

    $('#accept_account').on('click', function (evt) {
        evt.preventDefault();
        if ($('#formAccount').valid()) {
            var result = newAccount($('#password_account').val(), $('#password_confirm').val());
            if (result !== '') {
                $('#modalNew').modal('close');
                Materialize.toast('Cuenta creada con exito', 4000, 'rounded blue');
                $('#result-account').text('Tu cuenta es: ' + result);
                $('#modal-result').modal('open');
                addSelect('#emit_to', 'Elige una cuenta');
                addSelect('#transfer_from', 'De');
                addSelect('#transfer_to', 'Para');
                addSelect('#txt_burn', 'Elige una cuenta');
                addSelect('#emit_to_mlt', 'Elige una cuenta');
                addSelect('#txt_burnMltsg', 'Elige una cuenta');
                $('select').material_select();
                
                $.ajax({
                    url: '/ethWebjs/action-account',
                    type:'POST',
                    dataType: 'json',
                    data:{'address':result},
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        
                    }
                });
            }
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
});