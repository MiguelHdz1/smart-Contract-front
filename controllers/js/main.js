$(document).ready(function () {
    $('#once').load('../../views/sm.html');
    Materialize.toast($('.message').text(), 3000, 'rounded ' + $('.message').attr('id'));
    $(".button-collapse").sideNav();    

    $('#result').fadeOut();
    $('.modal').modal({
        dismissible: false,
        complete: function () {
            $('#formBurn')[0].reset();
            $('#formTransfer')[0].reset();
            $('#formEmit')[0].reset();
            $('#formAccount')[0].reset();
            $('#formEmitMlt')[0].reset();
            $('#formBurnMltsg')[0].reset();
            $('#result_account').text('');
            $('.amount_emit-error').text('');
            $('.password_decr-error').text('');
            $('.amount_transfer-error').text('');
            $('.password_transfer-error').text('');
            $('.amount_burn-error').text('');
            $('.password_burn-error').text('');
            $('.password_account-error').text('');
            $('.password_confirm-error').text('');
            $('.transfer_from-error').text('');
            $('.transfer_to-error').text('');
            $('.txt_burn-error').text('');
            $('.emit_to_mlt-error').text('');
            $('.amount_emit_mlt-error').text('');
            $('.password_decr_mlt-error').text('');
            $('.amount_burnMltsg-error').text('');
            $('.txt_burnMltsg-error').text('');
            $('.password_burnMltsg-error').text('');
            $('.emit_to-error').text('');
            $('#acc_sign-error').text('');
            $('#password-error').text('');
        }
    });

    $('#modal1').modal({
        dismissible: true
    });
    $('.cancel_modal').on('click', function (evt) {
        evt.preventDefault();
        $('.modal').modal('close');
        $('#result').text('');
        $('#txt_check').val('');
    });
    $('.total-balance').text('Total: ' + getTotalBalance());

    $('#update-balance').on('click', function (evt) {
        evt.preventDefault();
        $('.total-balance').text('Total: ' + getTotalBalance());
    });

    $('.direction-admin').on('click', function (evt) {
        evt.preventDefault();
        window.location.href = 'views/administrator.html';
    });
    addSelect('.select-acc', 'Elige una cuenta');
    $(document).ready(function () {
        $('select').material_select();
        $('.tooltipped').tooltip({delay: 50});
    });        
});
