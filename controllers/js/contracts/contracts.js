$(document).ready(function () {
    $('input#input_text, textarea#textarea1').characterCounter();
    var counter = 0;
    $(document).on('click', '#add-counter', function (evt) {
        evt.preventDefault();
        if (counter <= 3) {
            var addField = '<tr class="tr-section-'+(counter+2)+'"><td>\n\
                            </td>\n\
                            <td><div class="input-field">\n\
                            <input id="counter' + (counter + 2) + '" name="counter[]" type="text">\n\
                            <label for="counter' + (counter + 2) + '">Direcci&oacute;n ' + (counter + 2) + '</label>\n\
                            </div>\n\
                            <div class="col s12"><span class="red-text counter' + (counter + 2) + '-error error"></span></div></td>\n\
                            <td></td></tr>';
            $(this).parent().parent().parent().parent().append(addField);
            $(this).parent().parent().parent().hide();
            $(this).parent().parent().parent().fadeIn();
            $('.tooltipped').tooltip();
            counter++;
        } else {
            Materialize.toast('Haz alcanzado el limite de contrapartes', 4000, 'rounded red');
        }

    });
    
    $(document).on('click', '#drop-counter', function (evt) {
        evt.preventDefault();
        if (counter > 0) {            
            $('.tr-section-'+(counter+1)).remove();
            counter--;
        }
    });
    
    $(document).on('change','#allow',function(){
        if(this.checked){
            $('#conditionsAcc').val('1');
        }else{
            $('#conditionsAcc').val('0');
        }
    });
});