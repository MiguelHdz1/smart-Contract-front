$('.progress').css('visibility', 'hidden');
$(document).ready(function () {    
    $('#modal-password').modal({
        dismissible:false,
        complete: function(){
            $('#password_main').val('');
        }
    });
    
    
    $(document).on('click', '#create_contract', function (evt) {
        evt.preventDefault();
            $('#modal-password').modal('open');        
    });

    $(document).on('click', '#ready_contract', function (evt) {
        if ($('#smart-form').valid()) {
            $.ajax({
                url: '/ethWebjs/deploy-contract',
                type: 'POST',
                data: $('#smart-form').serialize(),
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if(data.status){
                        Materialize.toast(data.message, 4000, 'rounded blue');
                    }else{
                        Materialize.toast(data.message, 4000, 'rounded red');
                    }
                },error: function (jqXHR, textStatus, errorThrown) {
                    Materialize.toast('Ha ocurrido un error, verifica e intenta de nuevo', 4000, 'rounded red');
                }
            });        
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
            $('#modal-password').modal('close');
        }
    });
});
