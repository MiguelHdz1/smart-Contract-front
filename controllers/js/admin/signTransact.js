$(document).ready(function () {
    addTabletx();
    $('.tooltipped').tooltip();
    $('#modal-sign').modal({
        dismissible: false,
        complete: function () {
            $('#sign_form')[0].reset();
            $('.acc_sign-error').text('');
            $('.password-error').text('');
        }
    });
    $('.select-own').html(addOwners(getOwners()));
    $(document).on('click', '.approve-trans', function (evt) {
        evt.preventDefault();
        $('#id_transact').val($(this).attr('id'));
        $('#modal-sign').modal('open');
    });


    $('#accept_sign').on('click', function (evt) {
        evt.preventDefault();
        if ($('#sign_form').valid()) {
            if (signTransaction($('#acc_sign').val(), $('#password').val(), $('#id_transact').val())) {
                $('#modal-sign').modal('close');
            }
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
    
    $('#update-content').on('click',function (evt){
        evt.preventDefault();
        $('.total-balance').text('Total: ' + getTotalBalance());
        addTabletx();
        $('.tooltipped').tooltip({delay: 50});
    });

});