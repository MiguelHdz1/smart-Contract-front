$(document).ready(function () {

    addNoOwners('.owner-option','new_owner');
    $('#modal-addOwner').modal({
        dismissible: false,
        complete: function () {
            $('#addNew-form')[0].reset();
            $('.password_add-error').text('');
            $('.new_owner-error').text('');
        }
    });

    $('#update-content').on('click', function (evt) {
        evt.preventDefault();
        addNoOwners('.owner-option','new_owner');        
        $('#new_owner').material_select();       
        addNoOwners('.owner-option-sbs','substitute');
        $('#substitute').material_select();       
        addTabletOwner();        
    });

    $(document).on('click', '#addOwner', function (evt) {
        $('#modal-addOwner').modal('open');
    });

    $(document).on('click', '#confirm-add', function (evt) {
        evt.preventDefault();
        if ($("#addNew-form").valid()) {
            if (addNewOwner($('#new_owner').val(), $('#password_add').val())) {
                $('#modal-addOwner').modal('close');
            }
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
});