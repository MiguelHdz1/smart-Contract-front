$(document).ready(function () {

    $('#modal-revoke').modal({
        dismissible: false,
        complete: function () {
            $('#revoke_form')[0].reset();
            $('.acc_remove-error').text('');
            $('.password_remove-error').text('');
        }
    });

    $(document).on('click', '.remove-trans', function (evt) {
        evt.preventDefault();
        $('#id_transact_remove').val($(this).attr('id'));
        $('#modal-revoke').modal('open');
    });

    $('#accept_remove').on('click', function (evt) {
        evt.preventDefault();
        if ($('#revoke_form').valid()) {
            if (removeSign($('#acc_remove').val(), $('#password_remove').val(), $('#id_transact_remove').val())) {
                $('#modal-revoke').modal('close');
            }
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }

    });
});