$(document).ready(function () {
    addTabletOwner();
    $('#modal-confirm').modal({
        dismissible: false,
        complete:function(){
            $('#form-delete')[0].reset();
            $('.password_delete-error').text('');
        }
    });

    $(document).on('click', '.delete-owner', function (evt) {
        evt.preventDefault();
        $('#id_owner_del').val($(this).attr('data-bind'));
        $('#modal-confirm').modal('open');
    });

    $('#confirm-detete').on('click', function () {
        if ($('#form-delete').valid()) {
            if(deleteOwner($('#id_owner_del').val(), $('#password_delete').val())){
                $('#modal-confirm').modal('close');
            }
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
});