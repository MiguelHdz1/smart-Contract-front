$(document).ready(function () {
    
    var maxSize = 10;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        maxSize=5;
    }
    
    $('.direction-home').on('click',function (evt){
        evt.preventDefault();
        window.location.href='/index.html';
    });
        
    $('#demo').pagination({
        dataSource: arrayAccounts(),
        pageSize: maxSize,
        ulClassName: 'pagination',
        callback: function (data, pagination) {
            var content = '';
            $.each(data, function (key, value) {
                var str = value.toString();
                content += '<tr><td>' + str.substr(0, str.indexOf(',')) + '</td><td class="tbl-font">' + str.substr(str.indexOf(',')+1,str.length ) + '</td></tr>';                
            });
            $('.paginationjs-prev').html('<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_left</i></a></li>');
            $('.paginationjs-next').html('<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>');
            $('.content-pag').html(content);
        }
    });


});