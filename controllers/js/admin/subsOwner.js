$(document).ready(function (){
    addNoOwners('.owner-option-sbs','substitute');
    $('#modal-changeOwner').modal({
        dismissible: false,
        complete: function () {
            $('#subs-form')[0].reset();
            $('.password_subs-error').text('');
            $('.substitute-error').text('');
        }
    });
    
    $(document).on('click','.change-owner',function (evt){
        evt.preventDefault();
//        alert($(this).attr('data-bind'));        
        $('#id_substitute').val($(this).attr('data-bind'));
        $('#modal-changeOwner').modal('open');
    });
    
    $(document).on('click','#confirm-subs',function (evt){
        evt.preventDefault();        
        if($('#subs-form').valid()){
            if(changeOwner($('#id_substitute').val(),$('#substitute').val(),$('#password_subs').val())){
                $('#modal-changeOwner').modal('close');
            }
        }else{
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
});