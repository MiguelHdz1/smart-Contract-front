$(document).ready(function () {
    $("#subs-form").validate({
        ignore: "",
        rules: {
            substitute: {required: true},
            password_subs: {required: true}
        },
        messages: {
            substitute: {required: 'Campo requerido'},
            password_subs: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                      
        },
        success: function (error) {
            console.log('possible');
        }
    });
});