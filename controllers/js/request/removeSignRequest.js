$(document).ready(function () {
    $("#revoke_form").validate({
        ignore: "",
        rules: {
            acc_remove: {required: true},
            password_remove: {required: true}
        },
        messages: {           
            acc_remove: {required: 'Campo requerido'},
            password_remove: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                      
        },
        success: function (error) {
            console.log('possible');
        }

    });
});