$(document).ready(function () {
    $("#formBurnMltsg").validate({
        ignore: "",
        rules: {
            amount_burnMltsg: {required: true, number: true,min: 0.01},
            password_burnMltsg: {required: true},
            txt_burnMltsg: {required: true}
        },
        messages: {
            amount_burnMltsg: {required: 'Campo requerido', number: 'insertar solo números',min:'Ingresa un valor mayor o igual a 0.1'},
            password_burnMltsg: {required: 'Campo requerido'},
            txt_burnMltsg: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                      
        },
        success: function (error) {
            console.log('possible');
        }

    });
});