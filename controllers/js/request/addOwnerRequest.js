$(document).ready(function () {
    $("#addNew-form").validate({
        ignore: "",
        rules: {
            new_owner: {required: true},
            password_add: {required: true}
        },
        messages: {
            new_owner: {required: 'Campo requerido'},
            password_add: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                      
        },
        success: function (error) {
            console.log('possible');
        }
    });
});