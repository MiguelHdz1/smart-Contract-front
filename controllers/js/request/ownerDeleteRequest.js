$(document).ready(function () {
    $("#form-delete").validate({
        ignore: "",
        rules: {
            password_delete: {required: true},            
        },
        messages: {           
            password_delete: {required: 'Campo requerido'}        
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                      
        },
        success: function (error) {
            console.log('possible');
        }

    });
});