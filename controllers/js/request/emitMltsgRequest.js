$(document).ready(function () {    
    $("#formEmitMlt").validate({
        ignore:"",
        rules: {
            amount_emit_mlt: {required: true, number: true,min: 0.01},
            password_decr_mlt: {required: true},
            emit_to_mlt: {required: true}
        },
        messages: {
            amount_emit_mlt: {required: 'Campo requerido', number: 'insertar solo números',min:'Ingresa un valor mayor o igual a 0.1'},
            password_decr_mlt: {required: 'Campo requerido'},
            emit_to_mlt: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());            
        },
        success: function (error) {
            console.log('possible');
        }

    });

});