$(document).ready(function () {
    $("#formAccount").validate({
        rules: {
            password_account: {required: true,maxlength:12,minlength:5},
            password_confirm: {required: true,maxlength:12,minlength:5}
        },
        messages: {
            password_account: {required: 'Campo requerido', maxlength: 'Límite de caracteres 12',minlength:'Minimo de caracteres 5'},
            password_confirm: {required: 'Campo requerido', maxlength: 'Límite de caracteres 12',minlength:'Minimo de caracteres 5'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                      
        },
        success: function (error) {
            console.log('possible');
        }
    });
});