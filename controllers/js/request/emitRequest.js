$(document).ready(function () {    
    $("#formEmit").validate({
        ignore:"",
        rules: {
            amount_emit: {required: true, number: true,min: 0.01},
            password_decr: {required: true},
            emit_to: {required: true}
        },
        messages: {
            amount_emit: {required: 'Campo requerido', number: 'insertar solo números',min:'Ingresa un valor mayor o igual a 0.1'},
            password_decr: {required: 'Campo requerido'},
            emit_to: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());            
        },
        success: function (error) {
            console.log('possible');
        }

    });

});