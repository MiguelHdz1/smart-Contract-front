$(document).ready(function () {
    $("#formBurn").validate({
        ignore: "",
        rules: {
            amount_burn: {required: true, number: true,min: 0.01},
            password_burn: {required: true},
            txt_burn: {required: true}
        },
        messages: {
            amount_burn: {required: 'Campo requerido', number: 'insertar solo números',min:'Ingresa un valor mayor o igual a 0.1'},
            password_burn: {required: 'Campo requerido'},
            txt_burn: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                      
        },
        success: function (error) {
            console.log('possible');
        }

    });
});