$(document).ready(function () {
    $("#formTransfer").validate({     
        ignore: "",
        rules: {
            amount_transfer: {required: true, number: true,min: 0.01},
            password_transfer: {required: true},
            transfer_to:{required:true},
            transfer_from:{required:true}
        },
        messages: {
            amount_transfer: {required: 'Campo requerido', number: 'insertar solo números',min:'Ingresa un valor mayor o igual a 0.1'},
            password_transfer: {required: 'Campo requerido'},
            transfer_to: {required: 'Campo requerido'},
            transfer_from: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                  
            console.log(element.attr('name'));
        },
        success: function (error) {
            console.log('possible');
        }

    });    
});