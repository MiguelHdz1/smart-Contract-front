$(document).ready(function () {
    //addSelect('emit_to', 'Elige una cuenta');
    $('.emitUd').on('click', function (evt) {
        evt.preventDefault();
        $('#modalEmit').modal('open');
    });
    $('#accept_emmit').on('click', function (evt) {
        evt.preventDefault();
        if ($("#formEmit").valid()) {
            if (emitToken($('#amount_emit').val(),$('#emit_to').val(), $('#password_decr').val())) {
                $('#modalEmit').modal('close');
            }
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
});