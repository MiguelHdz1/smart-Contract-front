$(document).ready(function () {
    $(document).on('click', '.options', function (evt) {
        evt.preventDefault();
        var idCard = $(this).attr('id');
        $('#modal-operation').modal({
            dismissible: false,
            complete: function () {
                $('#operation-title').text('Espere...');
                $('#message-operation').text('');
            }
        });
        $('#modal-operation').modal('open');
        $.ajax({
            url: '/ethWebjs/verify-operation',
            type: 'POST',
            data: {typeOperation: $(this).attr('data-bind')},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#operation-title").fadeOut(function () {
                    $(this).text($('#' + idCard).children().children().children('.card-title').text()).fadeIn();
                });
                $("#message-operation").fadeOut(function () {
                    $('#message-operation').text(data.message).fadeIn();
                });                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#operation-title").fadeOut(function () {
                    $(this).text($('#' + idCard).children().children().children('.card-title').text()).fadeIn();
                });
                $("#message-operation").fadeOut(function () {
                    $('#message-operation').text('Ocurrio un error, intente de nuevo').fadeIn();
                });                 
            }
        });
    });
    
    var xmlString = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:consultaCEPLoteResponse xmlns:ns2="http://h2h.integration.spei.enlacefi.lgec.com/"><return><cda><url>http://www.banxico.org.mx/cep?i=90646&amp;s=20150825&amp;d=mesOHwpkbzh30xxBeC2936TLshd5oPwqKJXObY7%2B%2B2acgQdnAmH7FZm4Aqj0NaA524%2F2x3F2VBhmF8dHdN4mUQ%3D%3D</url></return><return><cda><cadenaOriginal>||01|22082016|22082016|112250|40014|STP|CONFIAMIGO|40|646180123000000009|CON131107QH1|SANTANDER|MARTIN DELFINO RUEDA LOPEZ|40|014180605577090317|RULM641204Q24|DesembolsoMRL0038|0.16|3000.00|00001000000304759430||</cadenaOriginal><conceptoPago>Desembolso MRL0038</conceptoPago><cuentaBeneficiario>014180605577090317</cuentaBeneficiario><cuentaOrdenante>646180123000000009</cuentaOrdenante><fechaCaptura>20160819</fechaCaptura><fechaOperacion>20160822</fechaOperacion><hora>11:22:48</hora><iva>0.16</iva><monto>3000.00</monto><nombreBeneficiario>MARTIN DELFINO RUEDA LOPEZ</nombreBeneficiario><nombreInstBeneficiaria>SANTANDER</nombreInstBeneficiaria><nombreInstOrdenante>STP</nombreInstOrdenante><nombreOrdenante>CONFIAMIGO</nombreOrdenante><referenciaNumerica>6950</referenciaNumerica><rfcCurpBeneficiario>RULM641204Q24</rfcCurpBeneficiario><rfcCurpOrdenante>CON131107QH1</rfcCurpOrdenante><selloDigital>QqTnxlMT7wjI66AcRF3KDTsGl4Tml3cYQTmuxYFYKO3R0CpHB/e8SU2rkKSa5F+7CbcAvq1HX6pOuFCNuQBgufH6IMS5vLB+O4BJHkGLlIcveO3KlL4n6nv1b3osMJWv0H8EYuByEnhVtTQXn38l0BB5ZUydtuDQOa3NVObE/HA=</selloDigital><serieCertificado/><tipoOperacion>C</tipoOperacion><tipoPago>1</tipoPago></cda><estadoConsulta>1</estadoConsulta><url>http://www.banxico.org.mx/cep?i=90646&amp;s=20150825&amp;d=mesOHwpkbzh30xxBeC2933bj1wN2fFgLo%2FBAdUmFcdfkJvs03DAYTL7WAK6dPDc8Oe5tGiKq7Zkurw%2BKm9rivQ%3D%3D</url></return></ns2:consultaCEPLoteResponse></soap:Body></soap:Envelope>';
    
    $(document).on('click','#xml-btn',function(evt){
        evt.preventDefault();
        $.ajax({
            url:'/ethWebjs/get-xml',
            type: 'POST',
            data:{'xml':xmlString},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                
            }
        });
    });  
});
