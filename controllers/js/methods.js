function arr_diff(a1, a2) {

    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    ;
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
}

function unlockAccount(account, password) {
    var result = false;
    try {
        web3.eth.defaultAccount = account;
        web3.personal.unlockAccount(web3.eth.defaultAccount, password);
        result = true;
    } catch (err) {
        result = false;
    }
    return result;
}

function checkBalances(Address) {
    var balance = scAddress.balanceOf(Address);
    return balance;
}

function Transfer(from, to, amount, password) {
    var result = false;
    if (unlockAccount(from, password)) {
        if (checkBalances(from) >= (amount * decimals)) {
            $('#transfer-loader').css('visibility', 'visible');
            scAddress.transfer.sendTransaction(to, parseFloat(amount) * decimals, {gas: 3100000, gasprice: '0x0'},
                    function (error, success) {
                        if (!error) {
                            Materialize.toast('Transferencia solicitada, ' + amount + ' unidades', 4000, 'rounded blue');
                            $.ajax({
                                url: '/ethWebjs/action-send',
                                type: 'POST',
                                data: {'from': from, 'to': to, 'amount': amount, 'hash': success},
                                dataType: 'json',
                                success: function (data, textStatus, jqXHR) {
                                    console.log(data);
                                },
                                error: function (jqXHR, textStatus, errorThrown) {

                                }
                            });
                        } else {
                            Materialize.toast('No se ha podido realizar la tranferencia', 4000, 'rounded red');
                        }
                    });
            result = true;
        } else {
            $('#transfer-loader').css('visibility', 'hidden');
            Materialize.toast('Saldo insuficiente', 4000, 'rounded red');
        }

    } else {
        $('#transfer-loader').css('visibility', 'hidden');
        Materialize.toast('Verificar contraseña', 4000, 'rounded red');
    }
    return result;
}

function addSelect(div, text) {
    var accounts = '<option value="" selected disabled>' + text + '</option>';
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $.each(web3.eth.accounts, function (key, value) {
            accounts += '<option value="' + value + '">' + value.substr(0, value.length - 35) + ' ... ' + value.substr(value.length - 8, value) + '</option>';
        });

    } else {
        $.each(web3.eth.accounts, function (key, value) {
            accounts += '<option value="' + value + '">' + value + '</option>';
        });
    }

    $(div).html(accounts);
}

function showAccounts() {
    var accounts = '';
    var colors = ['blue darken-3', 'red accent-4', 'teal darken-3', 'lime darken-2', 'orange darken-3',
        'deep-orange', 'blue-grey darken-4', 'brown darken-4', 'brown darken-2', 'green darken-3',
        'deep-purple darken-1', 'red lighten-1', 'indigo lighten-1', 'teal lighten-1', 'lime darken-4',
        'amber', 'amber darken-3', 'pink accent-2', 'pink darken-4', 'pink darken-3', 'blue lighten-2',
        'blue lighten-3', 'deep-purple lighten-2'];
    $.each(web3.eth.accounts, function (key, value) {
        accounts += '<a href="#" data-bind="' + value + '" class="det"><li class="collection-item avatar"><i class="material-icons circle ' + colors[Math.floor(Math.random() * colors.length) + 0  ] + '">account_balance_wallet</i>\n\
                   <span class="title green-text mobile-accounts">' + value.substr(0, value.length - 38) + ' ... ' + value.substr(value.length - 5, value) + '</span>\n\
                   <span class="title green-text browser-accounts">' + value + '</span>\n\
                   <p>Consultar cuenta...</p><a \n\
                   class="secondary-content">' + (key + 1) + '</a></li></a>';
    });
    $('.collection').html(accounts);
}

function arrayAccounts() {
    var accounts = [];
    $.each(web3.eth.accounts, function (key, value) {
        accounts[key] = [(key + 1) + ',' + value];
    });
    return accounts;
}

function emitToken(amount, to, password) {
    var result = false;
    if (unlockAccount(scAddress.ownerMain.call(), password)) {

        scAddress.updateTotalSupply(parseFloat(amount) * decimals, to, function (error, success) {
            if (success) {
                Materialize.toast('Emisión correcta ' + amount + ' unidades', 4000, 'rounded blue');
                $.ajax({
                    url: '/ethWebjs/action-emit',
                    type: 'POST',
                    data: {'hash': success, 'to': to, 'amount': amount},
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
            } else {
                Materialize.toast('Emisión no realizada', 4000, 'rounded red');
            }
        });
        result = true;
    } else {
        result = false;
        Materialize.toast('Verificar contraseña', 4000, 'rounded red');
    }
    return result;
}

function emit_multisig(to, amount, password) {
    var result = false;
    if (unlockAccount(to, password)) {
        scAddress.submitTransaction.sendTransaction(to, parseFloat(amount) * decimals, 1, {gas: 3100000, gasprice: '0x0'},
                function (error, success) {
                    if (success) {
                        Materialize.toast('Emisión solicitada, ' + amount + ' unidades espere confirmación', 4000, 'rounded blue');
                        $.ajax({
                            url: '/ethWebjs/action-emit-mult',
                            type: 'POST',
                            data:{'to':to,'amount':amount,'hash':success},
                            dataType: 'json',
                            success: function (data, textStatus, jqXHR) {
                                console.log(data);
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                
                            }
                        });
                    } else {
                        Materialize.toast('Emisión no realizada', 4000, 'rounded red');
                    }
                });
        result = true;
    } else {
        result = false;
        Materialize.toast('Verificar contraseña', 4000, 'rounded red');
    }
    return result;
}

function burn_multisig(amount, from, password) {
    var result = false;
    if (unlockAccount(from, password)) {
        if (checkBalances(from) >= (amount * decimals)) {
            scAddress.submitTransaction.sendTransaction(from, parseFloat(amount) * decimals, 0, {gas: 3100000, gasprice: '0x0'},
                    function (error, success) {
                        if (success) {
                            Materialize.toast('Quemado solicitado, ' + amount + ' unidades espere confirmación', 4000, 'rounded blue');
                            $.ajax({
                                url:'/ethWebjs/action-burn-mult',
                                type: 'POST',
                                data:{'from':from,'amount':amount,'hash':success},
                                dataType: 'json',
                                success: function (data, textStatus, jqXHR) {
                                    console.log(data);
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    
                                }
                            });
                        } else {
                            Materialize.toast('Quemado no realizado', 4000, 'rounded red');
                        }
                    });
            result = true;
        } else {
            Materialize.toast('Saldo insuficiente', 4000, 'rounded red');
        }
    } else {
        Materialize.toast('Verificar contraseña', 4000, 'rounded red');
    }
    return result;
}

function burn(amount, from, password) {
    var result = false;
    if (unlockAccount(scAddress.ownerMain.call(), password)) {
        if (checkBalances(from) >= (amount * decimals)) {
            scAddress.burn(parseFloat(amount) * decimals, from, function (error, success) {
                if (success) {
                    Materialize.toast('Quemado correcto ' + amount + ' unidades', 4000, 'rounded blue');
                    $.ajax({
                        url:'/ethWebjs/action-burn',
                        type: 'POST',
                        data: {'from':from,'amount':amount,'hash':success},
                        dataType: 'json',
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            
                        }
                    });
                } else
                    Materialize.toast('Quemado no realizado', 4000, 'rounded red');
            });
            result = true;
        } else {
            Materialize.toast('Saldo insuficiente', 4000, 'rounded red');
        }
    } else {
        Materialize.toast('Verificar contraseña', 4000, 'rounded red');
    }
    return result;
}

function newAccount(password, confirmPassword) {
    var account = '';
    if (password === confirmPassword && password !== '') {
        account += web3.personal.newAccount(password);
    } else {
        Materialize.toast('Verificar contraseña', 4000, 'rounded red');
    }
    return account;
}

function getTotalBalance() {
    var balance = scAddress.totalSupply.call() + '';
    var output = [balance.slice(0, balance.length - 2), '.', balance.slice(balance.length - 2)].join('');
    return output;
}

function getExecutedTransactions() {
    var pendingTransactions = [];
    var totalTransaction = scAddress.transactionCount.call();
    for (var i = 0; i < totalTransaction; i++) {
        var ej = scAddress.transactions.call(i);
        if (ej[4]) {
            pendingTransactions.push(ej);
        }
    }
    return pendingTransactions.reverse();
}

function getPendingTransactions() {
    var pendingTransactions = [];
    var totalTransaction = scAddress.transactionCount.call();
    for (var i = 0; i < totalTransaction; i++) {
        var txs = scAddress.transactions.call(i);
        if (!txs[4]) {
            pendingTransactions.push(txs);
        }
    }
    return pendingTransactions.reverse();
}

function getOwners() {
    var owners = scAddress.getOwners.call();
    //console.log(owners);
    return owners;
}

function isConfirmedByUser(idTransaction, address) {
    return scAddress.confirmations.call(idTransaction, address);
}

function addOwners(data) {
    var options = '<option value="" disabled selected>Firmar como...</option>';
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $.each(data, function (key, value) {
            options += '<option value="' + value + '">' + value.substr(0, value.length - 35) + ' ... ' + value.substr(value.length - 8, value) + '</option>';
        });
    } else {
        $.each(data, function (key, value) {
            options += '<option value="' + value + '">' + value + '</option>';
        });
    }
    return options;
}

function signTransaction(account, password, idTransaction) {
    var result = false;
    if (isConfirmedByUser(idTransaction, account) === false) {
        if (unlockAccount(account, password)) {
            scAddress.confirmTransaction.sendTransaction(idTransaction, {gas: 3100000, gasprice: '0x0'}, function (error, success) {
                if (success) {
                    Materialize.toast('Transacción firmada correctamente', 4000, 'rounded blue');
                    var tx = scAddress.transactions.call(idTransaction);
                } else
                    Materialize.toast('No se pudo firmar la transacción', 4000, 'rounded red');
            });
            result = true;

        } else {
            Materialize.toast('Verificar contraseña', 4000, 'rounded red');
        }
    } else {
        Materialize.toast('Esta cuenta ya ha firmado, intenta con otra', 4000, 'rounded red');
    }
    return result;
}


function removeSign(account, password, idTransaction) {
    var result = false;
    if (isConfirmedByUser(idTransaction, account)) {
        if (unlockAccount(account, password)) {
            scAddress.revokeConfirmation.sendTransaction(idTransaction, {gas: 3100000, gasprice: '0x0'}, function (error, success) {
                if (success) {
                    Materialize.toast('Firma removida correctamente', 4000, 'rounded blue');
                } else
                    Materialize.toast('No se pudo remover la firmar', 4000, 'rounded red');
            });
            result = true;

        } else {
            Materialize.toast('Verificar contraseña', 4000, 'rounded red');
        }
    } else {
        Materialize.toast('Esta cuenta aun no ha firmado, intenta con otra', 4000, 'rounded red');
    }
    return result;
}

function getRequiredConfirmations() {
    return  scAddress.required.call();
}

function deleteOwner(address, password) {
    var result = false;
    if (getOwners().length === Number(getRequiredConfirmations())) {
        Materialize.toast('Limite de dueños requeridos, no es posible eliminar', 4000, 'rounded red');
    } else {
        if (unlockAccount(scAddress.ownerMain.call(), password)) {
            scAddress.removeOwner(address, function (error, success) {
                if (success) {
                    Materialize.toast('Usuario eliminado correctamente', 4000, 'rounded blue');
                } else
                    Materialize.toast('No se pudo eliminar el usuario', 4000, 'rounded red');
            });
            result = true;
        } else {
            Materialize.toast('Verificar contraseña', 4000, 'rounded red');
        }
    }
    return result;
}

function getOwnerMain() {
    return scAddress.ownerMain.call();
}

function getNoOwners() {
    var owners = getOwners();
    var accounts = web3.eth.accounts;
    return arr_diff(owners, accounts);
}

function addNoOwners(section, id) {
    var options = '<select id="' + id + '" name="' + id + '"><option value="" selected disabled>Elegir cuenta</option>';
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $.each(getNoOwners(), function (key, value) {
            options += '<option value="' + value + '">' + value.substr(0, value.length - 35) + ' ... ' + value.substr(value.length - 8, value) + '</option>';
        });
    } else {
        $.each(getNoOwners(), function (key, value) {
            options += '<option value="' + value + '">' + value + '</option>';
        });
    }
    options += '</select><label>Elegir nuevo autorizador</label> ';
    $(section).html(options);
}

function getMaxOwners() {
    return Number(scAddress.max_owners.call());
}

function addNewOwner(address, password) {
    var result = false;
    if (getOwners().length === getMaxOwners()) {
        Materialize.toast('Limite de dueños requeridos, no es posible agregar', 4000, 'rounded red');
    } else {
        if (unlockAccount(scAddress.ownerMain.call(), password)) {
            scAddress.addOwner(address, function (error, success) {
                if (success) {
                    Materialize.toast('Usuario agregado correctamente', 4000, 'rounded blue');
                } else
                    Materialize.toast('No se pudo agregar el usuario', 4000, 'rounded red');
            });
            result = true;
        } else {
            Materialize.toast('Verificar contraseña', 4000, 'rounded red');
        }
    }
    return result;
}

function changeOwner(oldAddress, newAddress, password) {
    var result = false;
    if (unlockAccount(scAddress.ownerMain.call(), password)) {
        scAddress.replaceOwner(oldAddress, newAddress, function (error, success) {
            if (success) {
                Materialize.toast('Usuario sustituido correctamente', 4000, 'rounded blue');
            } else
                Materialize.toast('No se pudo sustituir el usuario', 4000, 'rounded red');
        });
        result = true;
    } else {
        Materialize.toast('Verificar contraseña', 4000, 'rounded red');
    }

    return result;
}