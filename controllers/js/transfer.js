$(document).ready(function () {
    $('#sendCoin').on('click', function (evt) {
        evt.preventDefault();
        $('#modalTransfer').modal('open');
    });


    //addSelect('transfer_from', 'De');
    //addSelect('transfer_to', 'Para');

    $('#accept_transfer').on('click', function (evt) {
        evt.preventDefault();
        if ($('#formTransfer').valid()) {
            if (Transfer($('#transfer_from').val(), $('#transfer_to').val(), $('#amount_transfer').val(), $('#password_transfer').val())) {
                $('#modalTransfer').modal('close');
            }
        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
});