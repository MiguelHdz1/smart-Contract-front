function addTabletOwner() {
    var maxSize = 10;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        maxSize=5;
    }
    
    $('#demo').pagination({
        dataSource: getOwners(),
        pageSize: maxSize,
        ulClassName: 'pagination',
        callback: function (data, pagination) {
            var content = '';
            var icon = '';
            $.each(data, function (key, value) {
                content += "<tr>";
                content += '<td>' + (key + 1) + '</td>';
                content += '<td class="div-balance">' + value + '</td>';
                content += '<td class="span-mobile">'+ value.substr(0, value.length - 38) + ' ... ' + value.substr(value.length - 5, value) + '</td>';
                if (value !== getOwnerMain()) {
                    content += '<td>' +
                            '<a href="" data-bind="' + value + '" class="btn tooltipped delete-owner red darken-3" data-position="top" data-delay="30" data-tooltip="Eliminar"><i class="material-icons">delete</i></a>' +
                            '<a href="" data-bind="' + value + '" class="btn tooltipped change-owner teal darken-4" data-position="top" data-delay="30" data-tooltip="Sustituir"><i class="material-icons">swap_vert</i></a>'
                            + '</td>';
                }else{
                    content += '<td>Administrador principal</td>';
                }
                content += "</tr>";
            });
            $('.paginationjs-prev').html('<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_left</i></a></li>');
            $('.paginationjs-next').html('<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>');
            $('.content-pag').html(content);
        }
    });
}