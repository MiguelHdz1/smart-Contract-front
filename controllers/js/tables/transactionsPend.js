function addTabletx() {
    var required = getRequiredConfirmations();
    var maxSize = 8;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        maxSize=5;
    }
    
    $('#demo').pagination({
        dataSource: getPendingTransactions(),
        pageSize: maxSize,
        ulClassName: 'pagination',
        callback: function (data, pagination) {
            var content = '';
            var icon = '';
            $.each(data, function (key, value) {                
                content += "<tr>";
                content += '<td>' + (parseInt(value[5]) + 1) + '</td>';
                content += '<td class="div-balance">' + value[1] + '</td>';
                content += '<td class="span-mobile">' + value[1].substr(0, value[1].length - 38) + ' ... ' + value[1].substr(value[1].length - 5, value[1]) + '</td>';
                content += '<td>' + ((value[3] == 0) ? 'Quemado' : 'Emisión') + '</td>';
                content += '<td>' + (parseFloat(value[2]) / decimals).toFixed(2) + '</td>';
                content += '<td>' + value[6] +' de '+required+ '</td>';
                content += '<td><i class="material-icons Tiny">' + (value[4] ? 'check' : 'lock') + '</i></td>';
                content += '<td>' + '<a href="" id="' + value[5] + '" class="btn light-blue darken-3 tooltipped approve-trans" data-position="top" data-delay="30" data-tooltip="Firmar"><i class="material-icons">border_color</i></a>' +
                        '<a href="" id="' + value[5] + '" class="btn tooltipped red darken-2 remove-trans" data-position="top" data-delay="30" data-tooltip="Remover firma"><i class="material-icons">format_clear</i></a>' +
                        '</td>';
                content += "</tr>";                
            });
            $('.paginationjs-prev').html('<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_left</i></a></li>');
            $('.paginationjs-next').html('<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>');
            $('.content-pag').html(content);
        }
    });
}