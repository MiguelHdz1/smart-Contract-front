function addTabletx() {
    var maxSize = 8;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        maxSize = 5;
    }

    $('#demo').pagination({
        dataSource: getExecutedTransactions(),
        pageSize: maxSize,
        ulClassName: 'pagination',
        callback: function (data, pagination) {
            var content = '';
            var icon = '';
            $.each(data, function (key, value) {
                //if (value[4] === false) {                
                content += "<tr>";
                content += '<td>' + (parseInt(value[5]) + 1) + '</td>';
                content += '<td class="div-balance">' + value[1] + '</td>';
                content += '<td class="span-mobile">' + value[1].substr(0, value[1].length - 38) + ' ... ' + value[1].substr(value[1].length - 5, value[1]) + '</td>';
                content += '<td>' + ((value[3] == 0) ? 'Quemado' : 'Emisión') + '</td>';
                content += '<td>' + (parseFloat(value[2]) / decimals).toFixed(2) + '</td>';
                content += '<td><i class="material-icons Tiny ' + (value[4] ? 'green-text' : 'red-text') + '">' + (value[4] ? 'check' : 'clear') + '</i></td>';
                content += "</tr>";
                //}
            });
            $('.paginationjs-prev').html('<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_left</i></a></li>');
            $('.paginationjs-next').html('<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>');
            $('.content-pag').html(content);
        }
    });
}