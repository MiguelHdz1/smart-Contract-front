$(document).ready(function () {
    //addSelect('emit_to_mlt', 'Elige una cuenta');
    $('#emitMltsg').on('click', function (evt) {
        evt.preventDefault();
        $('#modalEmitMlt').modal('open');
    });

    $('#accept_emmit_mlt').on('click', function (evt) {
        evt.preventDefault();
        if ($("#formEmitMlt").valid()) {
            if (emit_multisig($('#emit_to_mlt').val(), $('#amount_emit_mlt').val(), $('#password_decr_mlt').val())) {
                $('#modalEmitMlt').modal('close');
            }
        }else{
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
        }
    });
});