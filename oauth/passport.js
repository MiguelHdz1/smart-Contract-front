var bCrypt = require('bcrypt-nodejs');
module.exports = function (passport, users) {
    var User = users;
    var LocalStrategy = require('passport-local').Strategy;
    passport.serializeUser(function (users, done) {
        done(null, users.id);
    });
    passport.deserializeUser(function (id, done) {

        User.findById(id).then(function (users) {

            if (users) {

                done(null, users.get());

            } else {

                done(users.errors, null);

            }

        });

    });

    passport.use('local-signin', new LocalStrategy(
            {

                // by default, local strategy uses username and password, we will override with email

                usernameField: 'email',

                passwordField: 'password',

                passReqToCallback: true // allows us to pass back the entire request to the callback

            },
            function (req, email, password, done) {

                var User = users;

                var isValidPassword = function (userpass, password) {

                    return bCrypt.compareSync(password, userpass);

                };

                User.findOne({
                    where: {
                        email: email
                    }
                }).then(function (users) {

                    if (!users) {

                        return done(null, false, {
                            message: 'El correo no existe'
                        });

                    }

                    if (!isValidPassword(users.password, password)) {

                        return done(null, false, {
                            message: 'Credenciales incorrectas'
                        });

                    }


                    var userinfo = users.get();
                    return done(null, userinfo, req.flash('message','Bienvenido'),req.flash('type','success-msg'));


                }).catch(function (err) {
                    return done(null, false, {
                        message: 'Algo salió mal, intenta de nuevo'
                    });

                });


            }

    ));
};