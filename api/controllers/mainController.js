'use strict';
var accounts = require('./methods/accountsController');
var multsg = require('./methods/multsgController');
var owners = require('./methods/ownersController');
var transactions = require('./methods/transactionsController');
var models = require('./../../bd/models');
var bcrypt = require('bcrypt-nodejs');

exports.getBalance = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(accounts.checkBalance(req.params.idAccount));
};

exports.getAccounts = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(accounts.arrayAccounts());
};

exports.Transfer = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(transactions.Transfer(req.body.from, req.body.to, req.body.amount, req.body.password));
};

exports.emitToken = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(transactions.emitToken(req.body.amount, req.body.to, req.body.password));
};

exports.newAccount = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(accounts.newAccount(req.body.password, req.body.password_confirmation));
};

exports.burn = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(transactions.burn(req.body.amount, req.body.from, req.body.password));
};

exports.getTotalBalance = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(transactions.getTotalBalance());
};

exports.getOwners = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(owners.getOwners());
};

exports.getNoOwners = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(owners.getNoOwners());
};

exports.getMainOwner = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(owners.getOwnerMain());
};

exports.getMaxOwners = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(owners.getMaxOwners());
};

exports.getRequiredConfirmations = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.getRequiredConfirmations());
};

exports.isConfirmedByUser = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.isConfirmedByUser(req.params.transactionId, req.params.idAccount));
};

exports.emit_multisig = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.emit_multisig(req.body.to, req.body.amount, req.body.password));
};

exports.burn_multisig = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.burn_multisig(req.body.amount, req.body.from, req.body.password));
};

exports.addNewOwner = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(owners.addNewOwner(req.body.address, req.body.password));
};

exports.deleteOwner = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(owners.deleteOwner(req.body.address, req.body.password));
};

exports.changeOwner = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(owners.changeOwner(req.body.oldAddress, req.body.newAddress, req.body.password));
};

exports.signTransaction = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.signTransaction(req.body.account, req.body.password, req.body.idTransaction));
};

exports.removeSign = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.removeSign(req.body.account, req.body.password, req.body.idTransaction));
};

exports.getTransaction = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.getTransaction(req.params.id));
};

exports.getCountTransactions = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.getCountTransactions());
};

exports.getAllTransactions = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    res.send(multsg.getAllTransactions());
};

exports.formUser = function (req, res) {
    if (!req.user) {
        res.render('createUser');
    } else {
        res.redirect('/ethWebjs');
    }
};

exports.createUser = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    //console.log(req.body.password_user);
    models.Users.findOne({where: {email: req.body.email}}).then(function (users) {
        if (users) {
            res.status(202).send({'status': false, 'message': 'El usuario ya se encuentra registrado'});
        } else {
            var address = accounts.newAccount(req.body.password_user, req.body.password_confirm);
            if (address.status) {
                models.Users.create({
                    id:address.account,
                    email: req.body.email,
                    password: bcrypt.hashSync(req.body.password_user, bcrypt.genSaltSync(8), null),                    
                    status: '1'
                }).then(function (result) {
                    models.Profile.create({
                        name: req.body.name_user,
                        first_name: req.body.first_name,
                        last_name: req.body.last_name,
                        user_id: result.id
                    }).then(function (result) {
                        res.status(202).send({'status': true, 'message': 'Cuenta creada correctamente'});
                    }).catch(function (err) {
                        res.status(500).send({'message': 'El perfil no se ha podido agregar'});
                    });
                }).catch(function (err) {
                    res.status(500).send({'message': 'El usuario no se ha podido agregar'});
                });
            } else {
                res.status(500).send({'message': 'El usuario no se ha podido agregar'});
            }
        }
    });
};

exports.signup = function (req, res) {

    res.render('js/signup');
};

exports.checkAccounts = function (req, res) {

    res.render('administrator', {data: req.user});
};

exports.operations = function (req, res) {
    res.render('operations', {data: req.user});
};

exports.dataOwners = function (req, res) {

    res.render('owners', {data: req.user});
};

exports.ExecRequest = function (req, res) {

    res.render('exec_request', {data: req.user});
};

exports.pendingRequest = function (req, res) {

    res.render('pending_request', {data: req.user});
};

exports.index = function (req, res) {
    res.render('index', {data: req.user, message: req.flash('message'), type: req.flash('type')});
};

exports.swagger = function (req, res) {
    res.render('dist/index.html');
};

exports.signin = function (req, res) {
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
    if (!req.user) {
        res.render('js/signin.html', {error: req.flash('error')});
    } else {
        return res.redirect('/ethWebjs/');
    }
};

exports.logout = function (req, res) {
    req.session.destroy(function (err) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        res.redirect('/ethWebjs/');

    });

};