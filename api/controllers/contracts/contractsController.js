'use strict';

var $ = require('min-jquery');
var methods = require('../../conf/conf');
var accounts = require('../methods/accountsController');
var solc = require('solc');
var sources = require('./contractSources/salesContract');
var models = require('../../../bd/models');



module.exports = {

    manageContracts: function (req, res) {
        //console.log(req.user.id);
        models.contracts.findAll({
            attributes: ['id', 'hash', 'name'],
            where: {
                user_id: req.user.id,
                status: '1'
            }
        }).then(function (result) {
            res.render('manageContracts/index', {data: req.user, 'contracts': result});
        });

        //res.render('manageContracts/index', {data: req.user, 'contracts': contracts});
    },

    createContract: function (req, res) {
        res.render('manageContracts/create', {data: req.user});
    },
    
    showTemplates: function (req, res) {
        res.render('manageContracts/templates',{data:req.user});
    },

    deployContract: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        var contractSoruce = sources.contractSoruce;
        var compiled = solc.compile(contractSoruce, 1).contracts[':SalesContract'];
        var byteCode = '0x' + compiled.bytecode;
        var abiCompiled = JSON.parse(compiled.interface);
        var contract = methods.web3.eth.contract(abiCompiled);
        var nameProduct = req.body.nameProduct;
        var contractName = req.body.contractName;
        var ownerContract = req.body.ownerContract;
        var details = req.body.details;
        var priceProduct = req.body.priceProduct;
        var counter = req.body.counter;
        var conditions = req.body.conditions;
        var conditionStatus = (req.body.conditionsAcc == '1') ? true : false;
        var multiple = [];

        methods.web3.personal.unlockAccount(req.body.address, req.body.password_main, function (error, result) {
            if (!error) {
                models.Users.findOne({where: {id: ownerContract}}).then(function (ress) {
                    if (ress) {
                        models.Users.findAll({where: {id: counter}}).then(function (result) {
                            if (result) {
                                if ((counter.length) = (result.length)) {
                                    contract.new(
                                            nameProduct,
                                            contractName,
                                            ownerContract,
                                            details,
                                            priceProduct,
                                            counter,
                                            conditions,
                                            conditionStatus, 0, {
                                                from: methods.web3.eth.coinbase,
                                                data: byteCode,
                                                gas: 3100000,
                                                gasprice: '0x0'}, function (err, myContract) {
                                        if (!err) {
                                            if (!myContract.address) {
                                                console.log(myContract.transactionHash);
                                            } else {
                                                models.contracts.create({
                                                    name: contractName,
                                                    abi: JSON.stringify(abiCompiled),
                                                    byteCode: byteCode,
                                                    hash: myContract.address,
                                                    user_id: req.user.id,
                                                    status: '1'
                                                }).then(ressult => {
                                                    $.each(counter, function (key, value) {
                                                        multiple.push({id_contract: ressult.id, id_user: value});
                                                    });
                                                    models.participants.bulkCreate(multiple);
                                                    res.status(202).send({'status': true, 'message': 'Contrato creado con éxito'});
                                                }).catch(function (error) {
                                                    console.log(error);
                                                    res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                                                });
                                            }
                                        } else {
                                            console.log(err);
                                            res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                                        }
                                    });

                                } else {
                                    res.status(202).send({'status': false, 'message': 'Alguna de las contrapartes es un usuario invalido'});
                                }
                            } else {
                                res.status(202).send({'status': false, 'message': 'Los usuarios no estan registrados'});

                            }
                        }).catch(function (err) {
                            res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                            console.log(err);
                        });
                    } else {
                        res.status(202).send({'status': false, 'message': 'El dueño es un usuario invalido'});
                    }
                });
                console.log(result);
            } else {
                console.log(error);
            }
        });
    }

};
