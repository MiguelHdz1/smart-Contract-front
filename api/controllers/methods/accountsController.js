'use strict';

var $ = require('min-jquery');
var methods = require('../../conf/conf');

module.exports = {
    newAccount: function (password, confirmPassword) {
        let message = '';
        let result = false;
        let account = '';
        if (password.length >= 5 && password.length <= 10) {
            if (password === confirmPassword && password !== '') {
                result = true;
                message = 'Cuenta creada correctamente';
                account = methods.web3.personal.newAccount(password);
            } else {
                message = 'Verifique que ambas contraseñas sean iguales';
                account = null;
            }
        } else {
            message = "La contraseña debe ser mayor a 4 y menor a 11 caracteres";
            account = null;
        }
        return {'status': result, 'message': message, 'account': account};
    },

    unlockAccount: function (account, password) {
        var result = false;
        try {
            methods.web3.eth.defaultAccount = account;
            methods.web3.personal.unlockAccount(methods.web3.eth.defaultAccount, password,100000);
            result = true;
        } catch (err) {
            result = false;
        }
        return JSON.parse(result);
    },

    checkBalance: function (Address) {
        if (methods.web3.isAddress(Address)) {
            var balance = methods.scAddress.balanceOf(Address);
            return {'status':true,'adress': Address, 'balance': Number(balance)};
        } else {
            return {'status':false,'error': 'La cuenta no existe'};
        }
    },

    arrayAccounts: function () {
        var accounts = {};
        $.each(methods.web3.eth.accounts, function (key, value) {
            accounts[key] = {'id':(key + 1),'address': value};
        });
        return {"accounts": accounts};
    }
};