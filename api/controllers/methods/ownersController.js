'use strict';

var $ = require('min-jquery');
var methods = require('../../conf/conf');
var accounts = require('./accountsController');

module.exports = {
    arr_diff: function (a1, a2) {
        var a = [], diff = [];
        for (var i = 0; i < a1.length; i++) {
            a[a1[i]] = true;
        }
        ;

        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {
                delete a[a2[i]];
            } else {
                a[a2[i]] = true;
            }
        }

        for (var k in a) {
            diff.push(k);
        }
        return diff;
    },
    
    getOwners: function () {
        var owners = methods.scAddress.getOwners.call();
        //return {'owners':owners};
        return {'owners': $.extend({}, owners)};
    },

    getNoOwners: function () {
        var owners = $.extend([], this.getOwners().owners);
        var accounts = methods.web3.eth.accounts;
        return {'no-owners': $.extend({}, this.arr_diff(owners, accounts))};
    },

    getOwnerMain: function () {
        return {'main-owner': methods.scAddress.ownerMain.call()};
    },

    isOwner: function (address) {
        return {'is-owner': methods.scAddress.isOwner.call(address)};
    },

    getMaxOwners: function () {
        return {'max-owners': Number(methods.scAddress.max_owners.call())};
    },
    addNewOwner: function (address, password) {
        let result = false;
        let msg = '';
        var owners = $.extend([], this.getOwners().owners);

        if (methods.web3.isAddress(address)) {
            if (this.isOwner(address)['is-owner']) {
                msg = 'Este usuario ya es un dueño.';
            } else {
                if (owners.length === this.getMaxOwners()['max-owners']) {
                    msg = 'Limite de dueños requeridos, no es posible agregar';
                } else {
                    if (accounts.unlockAccount(methods.scAddress.ownerMain.call(), password)) {
                        msg = "Credenciales correctas";
                        methods.scAddress.addOwner(address, function (error, success) {
                            if (success) {
                                msg = 'Usuario agregado correctamente';
                            } else
                                msg = 'No se pudo agregar el usuario';
                        });
                        msg = 'Usuario agregado correctamente';
                        result = true;
                    } else {
                        msg = 'Verificar credenciales';
                    }
                }
            }
        } else {
            msg = 'La cuenta a agregar no existe';
        }


        return {'status': result, 'message': msg};
    },

    deleteOwner: function (address, password) {
        let result = false;
        let msg = '';
        var multsg = require('./multsgController');
        var owners = $.extend([], this.getOwners().owners);
        if (this.getOwnerMain()['main-owner'] == address) {
            msg = 'No es posible eliminar al dueño principal';
        } else {
            if (!this.isOwner(address)['is-owner']) {
                msg = 'Este usuario no es un dueño.';
            } else {
                if (owners.length == multsg.getRequiredConfirmations()['confirmations-required']) {
                    msg = 'Limite de dueños requeridos, no es posible eliminar';
                } else {
                    if (accounts.unlockAccount(methods.scAddress.ownerMain.call(), password)) {
                        msg = "Credenciales correctas";
                        methods.scAddress.removeOwner(address, function (error, success) {
                            if (success) {
                                msg = 'Usuario eliminado correctamente';
                            } else
                                msg = 'No se pudo eliminar el usuario';
                        });
                        msg = 'Usuario eliminado correctamente';
                        result = true;
                    } else {
                        msg = 'Verificar credenciales';
                    }
                }
            }
        }
        return {'status': result, 'message': msg};
    },

    changeOwner: function (oldAddress, newAddress, password) {
        let result = false;
        let msg = '';

        if (this.getOwnerMain()['main-owner'] == oldAddress) {
            msg = 'No es posible eliminar al dueño principal';
        } else {
            if (!this.isOwner(oldAddress)['is-owner']) {
                msg = 'El usuario a cambiar no es un dueño.';
            } else {
                if (methods.web3.isAddress(newAddress)) {
                    if (this.isOwner(newAddress)['is-owner']) {
                        msg = 'El usuario a agregar ya es un dueño.';
                    } else {
                        if (accounts.unlockAccount(methods.scAddress.ownerMain.call(), password)) {
                            msg = "Credenciales correctas";
                            methods.scAddress.replaceOwner(oldAddress, newAddress, function (error, success) {
                                if (success) {
                                    msg = 'Usuario sustituido correctamente';
                                } else
                                    msg = 'No se pudo sustituir el usuario';
                            });
                            msg = 'Usuario sustituido correctamente';
                            result = true;
                        } else {
                            msg = 'Verificar credenciales';
                        }
                    }
                } else {
                    msg = 'La cuenta a agregar no existe';
                }
            }
        }
        return {'status': result, 'message': msg};
    }
};