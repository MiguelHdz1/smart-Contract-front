'use strict';

var $ = require('min-jquery');
var methods = require('../../conf/conf');
var accounts = require('./accountsController');

module.exports = {
    Transfer: function (from, to, amount, password) {
        let result = false;
        let msg = "";
        let wsResponse = 505;
        if (accounts.unlockAccount(from, password)) {
            msg = "Credenciales correctas";
            if (accounts.checkBalance(from).balance >= (amount * methods.decimals)) {
                msg = "Balance suficiente";
                methods.scAddress.transfer(to, parseFloat(amount) * methods.decimals, function (error, success) {
                    if (success) {
                        msg = 'Tranferencia correcta ' + amount + ' unidades';
                    } else {
                        msg = 'Tranferencia no realizada';
                    }
                });
                result = true;
                msg = 'Tranferencia correcta ' + amount + ' unidades';
                wsResponse = 200;
            } else {
                msg = "Balance no sufuciente";
            }
        } else {
            msg = "Verificar credenciales";
        }
        return {'status': result, 'message': msg};
    },

    emitToken: function (amount, to, password) {
        let result = false;
        let msg = '';
        if (accounts.unlockAccount(methods.scAddress.ownerMain.call(), password)) {
            msg = "Credenciales correctas";
            methods.scAddress.updateTotalSupply(parseFloat(amount) * methods.decimals, to, function (error, success) {
                if (success) {
                    msg = "Emisión correcta " + amount + " unidades";
                } else {
                    msg = 'Emisión no realizada';
                }
            });
            msg = 'Emisión correcta ' + amount + ' unidades';
            result = true;
        } else {
            result = false;
            msg = "Verificar credenciales";
        }
        return {'status': result, 'message': msg};
    },

    burn: function (amount, from, password) {
        let result = false;
        let msg = '';
        if (accounts.unlockAccount(methods.scAddress.ownerMain.call(), password)) {
            msg = "Credenciales correctas";
            if (accounts.checkBalance(from).balance >= (amount * methods.decimals)) {
                msg = "Balance suficiente";
                methods.scAddress.burn(parseFloat(amount) * methods.decimals, from, function (error, success) {
                    if (success) {
                        msg = 'Quemado correcto ' + amount + ' unidades';
                    } else
                        msg = 'Quemado no realizado';
                });
                msg = 'Quemado correcto ' + amount + ' unidades';
                result = true;
            } else {
                msg = "Balance no sufuciente";
            }
        } else {
            msg = "Verificar credenciales";
        }
        return {'status': result, 'message': msg};
    },

    getTotalBalance: function () {
        let balance = methods.scAddress.totalSupply.call() + '';
        let output = [balance.slice(0, balance.length - 2), '.', balance.slice(balance.length - 2)].join('');
        return {'total-balance': parseFloat(output)};
    }
};