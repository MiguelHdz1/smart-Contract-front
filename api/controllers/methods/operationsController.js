'use strict';
var DOMParser = require('xmldom').DOMParser;
exports.verifyOperation = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');

    let operationId = req.body.typeOperation;
    let message = '';
    switch (operationId) {
        case '1':
            message = 'Estas intentando realizar un envio SPEI el cual lo provee STP.';
            break;
        case '2':
            message = 'Estas intentando realizar un envio de remesa el cual lo provee BTS.';
            break;
        case '3':
            message = 'Estas intentando realizar un pago de servicio el cual lo provee PROSA.';
            break;
        case '4':
            message = 'Estas intentando realizar una transferencia entre wallets.';
            break;
        case '5':
            message = 'Estas intentando realizar un retiro el cual lo provee BTS.';
            break;
        default:
            message = 'No se reconoce la opción, intenta de nuevo';
            break;
    }
    res.status(200).send({'status': true, 'message': message});
};

exports.sendResponse = function (req, res) {
    var fromDom = new DOMParser().parseFromString(req.body.xml, 'application/xml');
    res.status(200).send(xmlToJson(fromDom));
};

function xmlToJson(xml) {

    let obj = {};

    if (xml.nodeType === 1) { // element

        if (xml.attributes.length > 0) {
            obj['@attributes'] = {};
            for (let j = 0; j < xml.attributes.length; j += 1) {
                const attribute = xml.attributes.item(j);
                obj['@attributes'][attribute.nodeName] = attribute.nodeValue;
            }
        }
    } else if (xml.nodeType === 3) { // text
        obj = xml.nodeValue;
    }

    if (xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
        obj = xml.childNodes[0].nodeValue;
    } else if (xml.hasChildNodes()) {
        for (let i = 0; i < xml.childNodes.length; i += 1) {
            const item = xml.childNodes.item(i);
            const nodeName = item.nodeName;
            if (typeof (obj[nodeName]) === 'undefined') {
                obj[nodeName] = xmlToJson(item);
            } else {
                if (typeof (obj[nodeName].push) === 'undefined') {
                    const old = obj[nodeName];
                    obj[nodeName] = [];
                    obj[nodeName].push(old);
                }
                obj[nodeName].push(xmlToJson(item));
            }
        }
    }
    return obj;
};