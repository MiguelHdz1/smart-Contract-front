'use strict';

var $ = require('min-jquery');
var methods = require('../../conf/conf');
var accounts = require('./accountsController');

module.exports = {

    getRequiredConfirmations: function () {
        return  {'confirmations-required': Number(methods.scAddress.required.call())};
    },

    isConfirmedByUser: function (idTransaction, address) {
        return {'is-confirmed': methods.scAddress.confirmations.call(idTransaction, address)};
    },

    emit_multisig: function (to, amount, password) {
        let result = false;
        let msg = '';
        if (accounts.unlockAccount(to, password)) {
            msg = "Credenciales correctas";
            methods.scAddress.submitTransaction.sendTransaction(to, parseFloat(amount) * methods.decimals, 1, {gas: 3100000, gasprice: '0x0'},
                    function (error, success) {
                        if (success) {
                            msg = 'Emisión solicitada, ' + amount + ' unidades espere confirmación';
                        } else {
                            msg = 'Emisión no realizada';
                        }
                    });
            msg = 'Emisión solicitada, ' + amount + ' unidades espere confirmación';
            result = true;
        } else {
            result = false;
            msg = 'Verificar credenciales';
        }
        return {'status': result, 'message': msg};
    },

    burn_multisig: function (amount, from, password) {
        let result = false;
        let msg = '';
        if (accounts.unlockAccount(from, password)) {
            msg = "Credenciales correctas";
            if (accounts.checkBalance(from).balance >= (amount * methods.decimals)) {
                msg = "Balance suficiente";
                methods.scAddress.submitTransaction.sendTransaction(from, parseFloat(amount) * methods.decimals, 0, {gas: 3100000, gasprice: '0x0'},
                        function (error, success) {
                            if (success) {
                                msg = 'Quemado solicitado, ' + amount + ' unidades espere confirmación';
                            } else {
                                msg = 'Quemado no realizado';
                            }
                        });
                msg = 'Quemado solicitado, ' + amount + ' unidades espere confirmación';
                result = true;
            } else {
                msg = "Balance no sufuciente";
            }
        } else {
            msg = "Verificar credenciales";
        }
        return {'status': result, 'message': msg};
    },

    signTransaction: function (account, password, idTransaction) {
        let result = false;
        let msg = '';
        var owners = require('./ownersController');
        if (methods.scAddress.transactions.call(idTransaction)[0] == '0x0000000000000000000000000000000000000000') {
            msg = 'La transacción no existe';
        } else {
            if (!owners.isOwner(account)['is-owner']) {
                msg = 'El usuario no es un dueño.';
            } else {
                if (this.isConfirmedByUser(idTransaction, account)['is-confirmed'] === false) {
                    if (accounts.unlockAccount(account, password)) {
                        msg = "Credenciales correctas";
                        methods.scAddress.confirmTransaction.sendTransaction(idTransaction, {gas: 3100000, gasprice: '0x0'}, function (error, success) {
                            if (success) {
                                msg = 'Transacción firmada correctamente';
                            } else
                                msg = 'No se pudo firmar la transacción';
                        });
                        msg = 'Transacción firmada correctamente';
                        result = true;

                    } else {
                        msg = 'Verificar credenciales';
                    }
                } else {
                    msg = 'Esta cuenta ya ha firmado, intenta con otra';
                }
            }
        }
        return {'status': result, 'message': msg};
    },

    removeSign: function (account, password, idTransaction) {
        let result = false;
        let msg = '';
        if (this.isConfirmedByUser(idTransaction, account)['is-confirmed']) {
            if (accounts.unlockAccount(account, password)) {
                msg = "Credenciales correctas";
                methods.scAddress.revokeConfirmation.sendTransaction(idTransaction, {gas: 3100000, gasprice: '0x0'}, function (error, success) {
                    if (success) {
                        msg = 'Firma removida correctamente';
                    } else
                        msg = 'No se pudo remover la firmar', 4000, 'rounded red';
                });
                msg = 'Firma removida correctamente';
                result = true;

            } else {
                msg = 'Verificar credenciales';
            }
        } else {
            msg = 'Esta cuenta aun no ha firmado, intenta con otra';
        }
        return {'status': result, 'message': msg};
    },

    getTransaction: function (id) {
        let tsx = methods.scAddress.transactions.call(id);
        if (tsx[1] == '0x0000000000000000000000000000000000000000') {
            return {'status': false, 'message': 'La transaccion no existe'};
        } else {
            let transaction = {
                'id': tsx[5],
                'destination': tsx[1],
                'amount': parseFloat(tsx[2]),
                'type-operation': tsx[3],
                'executed': tsx[4],
                'signs': tsx[6]
            };
            return {'satus': true, transaction};
        }
    },

    getCountTransactions: function () {
        return {'total-transactions': Number(methods.scAddress.transactionCount.call())};
    },

    getAllTransactions: function () {
        var pendingTransactions = [];
        var totalTransaction = methods.scAddress.transactionCount.call();
        for (var i = 0; i < totalTransaction; i++) {
            var txs = methods.scAddress.transactions.call(i);
            pendingTransactions.push(txs);
        }
        return pendingTransactions.reverse();
    }
};