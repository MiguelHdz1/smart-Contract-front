'use strict';
var models = require('./../../bd/models');

exports.actionCreateAcc = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    models.requests.create({
        status: '1',
        json_information: '{"address": "' + req.body.address + '"}',
        hash: req.body.address,
        id_action: 1
    }).then(request => {
        res.status(202).send({'status': true, 'message': 'data inserted'});
        return true;
    }).catch(function (err) {
        res.status(202).send({'status': false, 'error': err});
    });
};

exports.actionEmit = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    models.requests.create({
        status: '0',
        json_information: '{"to": "' + req.body.to + '","amount":"' + req.body.amount + '"}',
        hash: req.body.hash,
        id_action: 2
    }).then(request => {
        res.status(202).send({'status': true, 'message': 'data inserted'});
        return true;
    }).catch(function (err) {
        res.status(202).send({'status': false, 'error': err});
    });
};

exports.actionSend = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    models.requests.create({
        status: '0',
        json_information: '{"from": "' + req.body.from + '","to": "' + req.body.to + '","amount":"' + req.body.amount + '"}',
        hash: req.body.hash,
        id_action: 3
    }).then(request => {
        res.status(202).send({'status': true, 'message': 'data inserted'});
        return true;
    }).catch(function (err) {
        res.status(202).send({'status': false, 'error': err});
    });
};

exports.actionBurn = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    models.requests.create({
        status: '0',
        json_information: '{"from": "' + req.body.from + '","amount":"' + req.body.amount + '"}',
        hash: req.body.hash,
        id_action: 4
    }).then(request => {
        res.status(202).send({'status': true, 'message': 'data inserted'});
        return true;
    }).catch(function (err) {
        res.status(202).send({'status': false, 'error': err});
    });
};

exports.actionEmitMultsg = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    models.requests.create({
        status: '0',
        json_information: '{"to": "' + req.body.to + '","amount":"' + req.body.amount + '"}',
        hash: req.body.hash,
        id_action: 5
    }).then(request => {
        res.status(202).send({'status': true, 'message': 'data inserted'});
        return true;
    }).catch(function (err) {
        res.status(202).send({'status': false, 'error': err});
    });
};

exports.actionBurnMultsg = function (req, res) {    
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    models.requests.create({
        status: '0',
        json_information: '{"from": "' + req.body.from + '","amount":"' + req.body.amount + '"}',
        hash: req.body.hash,
        id_action: 6
    }).then(request => {
        res.status(202).send({'status': true, 'message': 'data inserted'});
        return true;
    }).catch(function (err) {
        res.status(202).send({'status': false, 'error': err});
    });
};