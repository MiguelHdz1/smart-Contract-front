'use strict';

module.exports = function (app) {
    var controllers = require('../controllers/mainController');
    var actionController = require('../controllers/actionsController');
    var operationsController = require('../controllers/methods/operationsController');
    var contractController = require('../controllers/contracts/contractsController');

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/ethWebjs/login');
    }

    //VIEWS
    app.route('/ethWebjs').get(controllers.index);
    app.route('/ethWebjs/check-accounts').get(controllers.checkAccounts);
    app.route('/ethWebjs/data-owners').get(controllers.dataOwners);
    app.route('/ethWebjs/pending-request').get(controllers.pendingRequest);
    app.route('/ethWebjs/exec-request').get(controllers.ExecRequest);
    app.route('/ethWebjs/swagger').get(isLoggedIn, controllers.swagger);
    app.route('/ethWebjs/create-user').get(controllers.formUser);
    app.route('/ethWebjs/create-user').post(controllers.createUser);
    app.route('/ethWebjs/operations').get(controllers.operations);
    
    //OPERATIONS
    app.route('/ethWebjs/verify-operation').post(operationsController.verifyOperation);
    app.route('/ethWebjs/get-xml').post(operationsController.sendResponse);
    
    
    app.route('/ethWebjs/action-account').post(actionController.actionCreateAcc);
    app.route('/ethWebjs/action-emit').post(actionController.actionEmit);
    app.route('/ethWebjs/action-send').post(actionController.actionSend);
    app.route('/ethWebjs/action-burn').post(actionController.actionBurn);
    app.route('/ethWebjs/action-emit-mult').post(actionController.actionEmitMultsg);
    app.route('/ethWebjs/action-burn-mult').post(actionController.actionBurnMultsg);    

//    app.route('/ethWebjs/api').get('');


    ///////////ACCOUNTS//////////
    app.route('/ethWebjs/account-balance/:idAccount').get(controllers.getBalance);
    app.route('/ethWebjs/accounts').get(controllers.getAccounts);
    app.route('/ethWebjs/create-account').post(controllers.newAccount);


    /////////////OWNERS///////////
    app.route('/ethWebjs/owners').get(controllers.getOwners);
    app.route('/ethWebjs/no-owners').get(controllers.getNoOwners);
    app.route('/ethWebjs/main-owner').get(controllers.getMainOwner);
    app.route('/ethWebjs/max-owners').get(controllers.getMaxOwners);
    app.route('/ethWebjs/add-owner').post(controllers.addNewOwner);
    app.route('/ethWebjs/delete-owner').post(controllers.deleteOwner);
    app.route('/ethWebjs/change-owner').post(controllers.changeOwner);


    ////////////TRANSACTIONS///////////
    app.route('/ethWebjs/transfer').post(controllers.Transfer);
    app.route('/ethWebjs/emit').post(controllers.emitToken);
    app.route('/ethWebjs/burn').post(controllers.burn);
    app.route('/ethWebjs/total-balance').get(controllers.getTotalBalance);


    /////////MULTISIGNATURE TRANSACTIONS////////////
    app.route('/ethWebjs/confirm-required').get(controllers.getRequiredConfirmations);
    app.route('/ethWebjs/is-confirmed/:transactionId/:idAccount').get(controllers.isConfirmedByUser);
    app.route('/ethWebjs/emit-multsg').post(controllers.emit_multisig);
    app.route('/ethWebjs/burn-multsg').post(controllers.burn_multisig);
    app.route('/ethWebjs/sign').post(controllers.signTransaction);
    app.route('/ethWebjs/remove-sign').post(controllers.removeSign);
    app.route('/ethWebjs/transaction/:id').get(controllers.getTransaction);
    app.route('/ethWebjs/total-tsx').get(controllers.getCountTransactions);
    app.route('/ethWebjs/all-tsx').get(controllers.getAllTransactions);
    
    
    //////////////////////MANAGE CONTRACTS ////////////////////////
    app.route('/ethWebjs/manage-contracts').get(isLoggedIn,contractController.manageContracts);
    app.route('/ethWebjs/manage-contracts/create').get(isLoggedIn,contractController.createContract);

    app.route('/ethWebjs/deploy-contract').post(contractController.deployContract);
    app.route('/ethWebjs/contracts').get(isLoggedIn,contractController.showTemplates);
    app.route('/ethWebjs/view/:id').get(controllers.getTransaction);
    

    app.use(function (req, res) {
        res.status(404).send('404: Page not Found');
    });
};