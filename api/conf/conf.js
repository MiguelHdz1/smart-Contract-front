var Web3 = require('web3');
var abi = require('../conf/abi');
var web3 = new Web3(Web3.givenProvider || "http://192.168.2.47:8545");

exports.web3 = web3;
web3.setProvider(new web3.providers.HttpProvider("http://192.168.2.47:8545"));
var contractSource = web3.eth.contract(abi.abi);
//console.log(abi);
var scAddress = contractSource.at('0xf3b38230b59f7b8e4760d25ddc6bcea1db1690ec');
exports.scAddress = scAddress;

var decimals = parseInt(100);
exports.decimals = decimals;