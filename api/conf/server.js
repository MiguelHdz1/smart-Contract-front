const express = require('express');
const port = 8000;
const app = express();
const bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');
var env = require('dotenv').load();
var exphbs = require('express-handlebars');
var models = require('./../../bd/models');
var methods = require('./conf');
var $ = require('min-jquery');

app.use(session({secret: 'keyboard cat', resave: true, saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var path = require('path');
app.set('views', path.join(__dirname, '..', '..', 'views'));
app.engine('hbs', exphbs({
    extname: '.hbs'
}));
app.engine('html', exphbs({
    extname: '.html'
}));
app.set('view engine', '.hbs');
app.use(express.static(path.join(__dirname, '..', '..', 'public')));
app.use(express.static(path.join(__dirname, '..', '..', 'controllers')));
app.use(express.static(path.join(__dirname, '..', '..', 'views')));
var flash = require('connect-flash');

app.use(flash());

require('./../../oauth/passport')(passport, models.Users);

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function (req, res, next) {
    res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
});
app.listen(port, () => {
    console.log('Now you are connected on port: ' + port);
});

var filter = methods.web3.eth.filter('latest');

filter.watch(function (error, result) {
    var notExec = models.requests.findAll({
        attributes: ['hash'],
        where: {
            status: '0'
        }
    });

    notExec.then(function (result) {
        $.each(result, function (key, value) {
            if(methods.web3.eth.getTransactionReceipt(value.hash)!=null){
                models.requests.update({
                    status:'1'
                },{
                    where:{
                        hash: value.hash
                    }
                });
            }            
        });
    });
});

require('../routes/oauth.js')(app, passport);
require('../routes/routes')(app, {});