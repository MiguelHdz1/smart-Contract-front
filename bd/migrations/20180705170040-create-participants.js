'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('participants', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_contract: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
            model: 'contracts',
            key: 'id'
        }
      },
      id_user: {
        type: Sequelize.STRING,
        onDelete: 'CASCADE',
        references: {
            model: 'users',
            key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('participants');
  }
};