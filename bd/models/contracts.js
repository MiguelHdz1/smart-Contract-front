'use strict';
module.exports = (sequelize, DataTypes) => {
  var contracts = sequelize.define('contracts', {
    name: DataTypes.STRING,
    abi: DataTypes.STRING,
    byteCode: DataTypes.STRING,
    hash: DataTypes.STRING,
    user_id: DataTypes.STRING, 
    status: DataTypes.CHAR
  }, {});
  contracts.associate = function(models) {
    // associations can be defined here
  };
  return contracts;
};