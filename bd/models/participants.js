'use strict';
module.exports = (sequelize, DataTypes) => {
  var participants = sequelize.define('participants', {
    id_contract: DataTypes.INTEGER,
    id_user: DataTypes.STRING
  }, {});
  participants.associate = function(models) {
    // associations can be defined here
  };
  return participants;
};