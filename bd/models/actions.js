'use strict';
module.exports = (sequelize, DataTypes) => {
  var actions = sequelize.define('actions', {      
      name: DataTypes.STRING,
      status: DataTypes.CHAR  
  }, {});
  actions.associate = function(models) {
    // associations can be defined here
  };
  return actions;
};