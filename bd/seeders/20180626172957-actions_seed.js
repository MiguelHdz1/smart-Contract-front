'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('actions',
                [
                    {
                        name: 'create_account',
                        status: '1'
                    },
                    {
                        name: 'emit',
                        status: '1'
                    },
                    {
                        name: 'send',
                        status: '1'
                    },
                    {
                        name: 'burn',
                        status: '1'
                    },
                    {
                        name: 'emit_multisignature',
                        status: '1'
                    },
                    {
                        name: 'burn_multisignature',
                        status: '1'
                    }
                ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('actions', null, {});
    }
};
